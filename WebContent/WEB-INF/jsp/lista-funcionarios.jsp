<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista de Funcionários</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilo.css"/>
</head>
<body>
	<c:import url="cabecalho.jsp"/>
		<table border="1" style="border-collapse: collapse;">
			<tr>
				<th>N°</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>CPF</th>
				<th>Senha</th>
				<th colspan="3">Ações</th>
			</tr>
			<c:forEach var="funcionario" items="${funcionarios}" varStatus="id">
				<tr bgcolor="#${id.count % 2 == 0 ? '94FFF7' : 'D7D7FF' }">
					<td>${id.count}</td>
					<td>${funcionario.nome}</td>
					<td>
						<c:if test="${not empty funcionario.email}">
							<a href="mailto:${funcionario.email }">${funcionario.email }</a>
						</c:if>
						<c:if test="${empty funcionario.email}">
							Email não informado.
						</c:if>
					</td>
					<td>${funcionario.cpf}</td>
					<td>${funcionario.senha}</td>
					<td>
						<a href="mvc?logica=RemoveFuncionarioLogica&id=${funcionario.id}">Excluir</a>
						<a href="mvc?logica=ExibeFuncionarioLogica&id=${funcionario.id}">Editar</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	<c:import url="rodape.jsp"/>
</body>
</html>