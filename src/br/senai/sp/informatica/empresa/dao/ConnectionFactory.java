package br.senai.sp.informatica.empresa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * @author Matheus Santos
 *
 */
public class ConnectionFactory {

	public Connection getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://172.16.7.23/empresat", "tarde", "tarde");
		} catch (SQLException | ClassNotFoundException e) {
			throw new RuntimeException(e);

		}
	}
}