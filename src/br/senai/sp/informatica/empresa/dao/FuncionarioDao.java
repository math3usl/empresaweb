package br.senai.sp.informatica.empresa.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresa.model.Funcionario;

/**
 * @author Matheus Santos
 *
 */
public class FuncionarioDao {

	private Connection connection;

	public FuncionarioDao() {
		connection = new ConnectionFactory().getConnection();
	}

	public void salva(Funcionario funcionario) {

		String sql = null;

		if (funcionario.getId() != null) {
			sql = "UPDATE Funcionario SET nome = ?, email = ?, cpf = ?, senha = ? WHERE id = ?";
		} else {
			sql = "INSERT INTO Funcionario (nome, email, cpf, senha) VALUES (?, ?, ?, ?)";
		}

		try {
			PreparedStatement stmt = connection.prepareStatement(sql);

			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getCpf());
			stmt.setString(4, funcionario.getSenha());
			
			if (funcionario.getId() != null) {
				stmt.setLong(5, funcionario.getId());
			}
			
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public List<Funcionario> getLista() {

		try {
			List<Funcionario> funcionarios = new ArrayList<>();

			PreparedStatement stmt = connection.prepareStatement("SELECT * FROM Funcionario");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				Funcionario funcionario = new Funcionario();
				funcionario.setId(rs.getLong("id"));
				funcionario.setNome(rs.getString("nome"));
				funcionario.setEmail(rs.getString("email"));
				funcionario.setCpf(rs.getString("cpf"));
				funcionario.setSenha(rs.getString("senha"));

				funcionarios.add(funcionario);
			}

			rs.close();
			stmt.close();

			return funcionarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public void excluir(Funcionario funcionario) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM Funcionario WHERE id = ?");
			stmt.setLong(1, funcionario.getId());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}