package br.senai.sp.informatica.empresa.mvc.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import br.senai.sp.informatica.empresa.mvc.logica.Logica;

/**
 * @author Matheus Santos
 *
 */
@WebServlet("/mvc")
public class ServletController extends HttpServlet{

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String parametro = req.getParameter("logica");
		
		String className = "br.senai.sp.informatica.empresa.mvc.logica." + parametro;
		
		try {
			Class classe = Class.forName(className);
			
			Logica logica = (Logica) classe.newInstance();
			
			String pagina = logica.executa(req, res);
			
			req.getRequestDispatcher(pagina).forward(req, res);
		} catch (Exception e) {
			throw new ServletException("A l�gica de neg�cios causou uma exce��o", e);
		}
		
	}
}