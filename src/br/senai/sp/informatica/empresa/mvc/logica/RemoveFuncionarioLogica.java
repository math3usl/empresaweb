package br.senai.sp.informatica.empresa.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresa.dao.FuncionarioDao;
import br.senai.sp.informatica.empresa.model.Funcionario;

/**
 * @author Matheus Santos
 *
 */
public class RemoveFuncionarioLogica implements Logica {

	@Override
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
	
		long id = Long.parseLong(req.getParameter("id"));
		
		Funcionario funcionario = new Funcionario();
		funcionario.setId(id);
		
		FuncionarioDao dao = new FuncionarioDao();
		dao.excluir(funcionario);
		
		System.out.println("Excluindo o funcionário.");
		
		return "mvc?logica=ListaFuncionariosLogica";
	}
}