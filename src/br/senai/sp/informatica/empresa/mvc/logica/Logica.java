package br.senai.sp.informatica.empresa.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Matheus Santos
 *
 */
public interface Logica {
	public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception;
}