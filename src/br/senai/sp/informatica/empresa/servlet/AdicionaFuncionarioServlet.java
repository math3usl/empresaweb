package br.senai.sp.informatica.empresa.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.senai.sp.informatica.empresa.model.Funcionario;
import br.senai.sp.informatica.empresa.dao.FuncionarioDao;

/**
 * @author Matheus Santos
 *
 */
@WebServlet("/adicionaFuncionario")
public class AdicionaFuncionarioServlet extends HttpServlet{
	
		@Override
		protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
			PrintWriter out = res.getWriter();
			
			String nome = req.getParameter("nome");
			String email = req.getParameter("email");
			String cpf = req.getParameter("cpf");
			String senha = req.getParameter("senha");			
					
			Funcionario funcionario = new Funcionario();
			funcionario.setNome(nome);
			funcionario.setEmail(email);
			funcionario.setCpf(cpf);
			funcionario.setSenha(senha);
			
			FuncionarioDao dao = new FuncionarioDao();			

			dao.salva(funcionario);
			
			RequestDispatcher dispatcher = req.getRequestDispatcher("/funcionario-adicionado.jsp");
			dispatcher.forward(req, res);
		}
}
